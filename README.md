# Clube GDP VS Code Theme

[![Version](https://vsmarketplacebadge.apphb.com/version-short/patrickf-clubegdp.clubegdp-dark-theme.svg)](https://marketplace.visualstudio.com/items?itemName=patrickf-clubegdp.clubegdp-dark-theme)
[![Downloads](https://vsmarketplacebadge.apphb.com/downloads-short/patrickf-clubegdp.clubegdp-dark-theme.svg)](https://marketplace.visualstudio.com/items?itemName=patrickf-clubegdp.clubegdp-dark-theme)
[![Rating](https://vsmarketplacebadge.apphb.com/rating-star/patrickf-clubegdp.clubegdp-dark-theme.svg)](https://marketplace.visualstudio.com/items?itemName=patrickf-clubegdp.clubegdp-dark-theme)

This is a theme branded with the colors of Clube Gazeta do Povo. It is based on WCAG standards to provide better accessibility and readability, mainly on dark environments. All choices were based to provide better experience through long hours of code.

# Installation

1.  Install [Visual Studio Code](https://code.visualstudio.com/)
2.  Launch Visual Studio Code
3.  Choose **Extensions** from menu
4.  Search for `clubegdp`
5.  Click **Install** to install it
6.  Click **Reload** to reload the Code
7.  From the menu bar click: Code > Preferences > Color Theme > **Clube GdP**

**Enjoy!**
